# Changelog

#### 2.4.1

- Enhancement: update underlying libraries (s2, abseil) to profit from performance improvements and possible bugfixes.

### 2.4.0

- Feature: add `<` instance for `:: CircleRegion` to `Data.Geo.Ord`.

#### 2.3.1

- Chore: accept `base` `3.0`.

#### 2.3.0

- Feature: add `==` instances and `genShow` derivations for `RectRegion` and `CircleRegion`.

#### 2.2.3

- Chore: add dependencies on generic-print, json and posix to make compilation test succeed.

#### 2.2.2

- Chore: Support base ^2.0.

#### 2.2.1

- Chore: update `s2geometry` and `abseil-cpp` dependency.

### 2.2.0

- Feature: add `Data.Geo.GenHash` providing `gHash` instances for all types defined in `Data.Geo`.

### 2.1.0

- Feature: add `genShow`/`ggen` instances of `Region` and `RegionValue`.
- Fix: `instance < Position`.

## 2.0.0

- Change: Check that latitude and longitude are in the correct range when generating Position values. Requires
          to change the type of `position` to result in a `MaybeError` value.

#### 1.1.2

- Fix: bug in `rectRegionToBoundaryLines` causing erroneous lines to be returned by the function for the west/south boundaries.

#### 1.1.1

- Chore: switch to `abseil-cpp` branch `lts_2022_06_23`.

### 1.1.0

- Feature: add function to create lines and check if the lines intersect with other lines or if the lines intersect with the boundary lines of a `:: RectRegion`.

#### 1.0.2

- Chore: upgrade base-compiler-itasks version constraint from =1.0.0 to ^1.0.0 || ^2.0.0

#### 1.0.1

- Robustness: add triangle inequality property to `Data.Geo.distanceBetween`

## 1.0.0

- Initial version
