implementation module Data.Geo

import Control.Applicative
import Control.GenBimap, Control.Monad
import Data.Encoding.GenBinary, Data.Error, Data.Func, Data.Functor, Data.GenEq, Data.GenFDomain, Data.GenLexOrd
import Data.GenDefault, Data.Maybe
import StdEnv => qualified foldl
import Text => qualified join
import Text.GenJSON, Text.HTML, Text.GenPrint

import Data.Geo._S2

gLexOrd{|Position|} x y
	| latX == latY = lngX =?= lngY
	| otherwise    = latX =?= latY
where
	latX = latitudeOf x
	lngX = longitudeOf x
	latY = latitudeOf y
	lngY = longitudeOf y

position :: !Real !Real -> MaybeError String Position
position lat lng
	| lat > 90.0 = Error "The latitude should be smaller or equals 90°."
	| lat < -90.0 = Error "The latitude should be larger or equals -90°."
	| lng > 180.0 = Error "The longitude should be smaller or equals 180°."
	| lng < -180.0 = Error "The longitude should be larger or equals -180°."
	= Ok $ Position (s2LatLng (round lat) (round lng))
where
	round :: !Real -> Real
	round x = (fromInt $ toInt (x * POSITION_PRECISION)) / POSITION_PRECISION

positionCorrespondingToHoursMinutesSeconds ::
	!Int !Int !Int !Int !LatDirection !Int !Int !Int !Int !LngDirection -> MaybeError String Position
positionCorrespondingToHoursMinutesSeconds latH latM latS latPrec latDir lngH lngM lngS lngPrec lngDir =
	position (if (latDir =: North) lat (~lat)) (if (lngDir =: East) lng (~lng))
where
	lat = toReal latH + toReal latM / 60.0 + toReal latS / (toReal $ 10^latPrec) / 3600.0
	lng = toReal lngH + toReal lngM / 60.0 + toReal lngS / (toReal $ 10^lngPrec) / 3600.0

latitudeOf :: !Position -> Real
latitudeOf (Position latLng) = s2LatLngGetLat latLng

longitudeOf :: !Position -> Real
longitudeOf (Position latLng) = s2LatLngGetLng latLng

latitudeHoursMinutesSecondsOf :: !Int !Position -> (!Int, !Int, !Int, !LatDirection)
latitudeHoursMinutesSecondsOf precision position = (hours, minutes, seconds, if isNorth North South)
where
	(hours, minutes, seconds, isNorth) = hoursMinutesSecondsOf precision $ latitudeOf position

longitudeHoursMinutesSecondsOf :: !Int !Position -> (!Int, !Int, !Int, !LngDirection)
longitudeHoursMinutesSecondsOf precision position = (hours, minutes, seconds, if isEast East West)
where
	(hours, minutes, seconds, isEast) = hoursMinutesSecondsOf precision $ longitudeOf position

hoursMinutesSecondsOf :: !Int !Real -> (!Int, !Int, !Int, !Bool)
hoursMinutesSecondsOf precision degrees = (hours, minutes, seconds, direction)
where
	hours      = if carryMins (hours` + 1) hours`
	hours`     = entier absDegrees
	minutes    = if carryMins 0 minutes`
	minutes`   = if carrySecs (minutes`` + 1) minutes``
	minutes``  = entier (60.0 * (absDegrees - toReal hours`))
	carryMins  = minutes` == 60
	seconds    = if carrySecs 0 seconds`
	seconds`   = toInt $ 3600.0 * (absDegrees - toReal hours` - (toReal minutes`` / 60.0)) * toReal (10^precision)
	carrySecs  = seconds` == 60 * (10^precision)
	direction  = degrees >= 0.0
	absDegrees = abs degrees

instance == CellId
where
	(==) :: !CellId !CellId -> Bool
	(==) a b =
		code inline {
			.d 2 0
			jsr eqAC
			.o 0 1 b
		}

instance < CellId
where
	(<) :: !CellId !CellId -> Bool
	(<) a b =
		code inline {
			.d 2 0
			jsr cmpAC
			.o 0 1 i
			pushI 0
			gtI
		}

idOfCellIncluding :: !Position -> CellId
idOfCellIncluding (Position latLng) = CellId (cellIdOfLatLng latLng)

longitudeInTheMiddleBetween :: !Real !Real -> Real
longitudeInTheMiddleBetween lng1 lng2 | abs (lng1 - midLng) > 90.0
                                          | midLng > 0.0 = midLng - 180.0
                                          | otherwise    = midLng + 180.0
                                      | otherwise = midLng
where
    midLng :: Real
    midLng = (lng1 + lng2) / 2.0

instance == Position
where
	(==) :: !Position !Position -> Bool
	(==) a b =
		code inline {
			.d 2 0
			jsr eqAC
			.o 0 1 b
		}

instance < Position
where
	(<) :: !Position !Position -> Bool
	(<) x y = (x =?= y) === LT

instance toString Position where
	toString position =
		concat
			[ coordinateToString (latitudeHoursMinutesSecondsOf  strReprPrecision position) False , " "
			, coordinateToString (longitudeHoursMinutesSecondsOf strReprPrecision position) False
			]

instance html Position where
	html position =
		html
			[ Html $ coordinateToString (latitudeHoursMinutesSecondsOf  strReprPrecision position) True , Html "&nbsp;"
			, Html $ coordinateToString (longitudeHoursMinutesSecondsOf strReprPrecision position) True
			]

derive gDefault        Position

coordinateToString :: !(!Int, !Int, !Int, !direction) !Bool -> String | toString direction
coordinateToString (hours, minutes, seconds, direction) isHtml =
	concat
		[ toString hours, "°", space, lpad (toString minutes) 2 '0', acc, space
		, secondsStr % (0, 1), ".", secondsStr % (2, 1 + strReprPrecision), doubleAcc, space , toString direction
		]
where
	secondsStr = lpad (toString seconds) (2 + strReprPrecision) '0'
	space     = if isHtml "&nbsp;" " "
	acc       = "′"
	doubleAcc = "″"

strReprPrecision = 2

instance toString LatDirection where
	toString North = "N"
	toString South = "S"

instance toString LngDirection where
	toString East = "E"
	toString West = "W"

JSONEncode{|Position|} inField pos = JSONEncode{|*|} inField (latitudeOf pos, longitudeOf pos)
JSONDecode{|Position|} inField jsonNodes
	# (latLng, rest) = JSONDecode{|*|} inField jsonNodes
	= (latLng >>= \(lat, lng) = error2mb $ position lat lng, rest)

distanceBetween :: !Position !Position -> Real
distanceBetween (Position pos1) (Position pos2) = distanceBetweenLatLng pos1 pos2

smallestRectCovering :: ![Position] -> ?RectRegion
smallestRectCovering allPos=:[fstPosition: positions] =
	?Just {northWest = fromOkWithError $ position maxLat minLng, southEast = fromOkWithError $ position minLat maxLng}
where
    (minLat, maxLat) = 'StdEnv'.foldl
        (\(minLat, maxLat) pos -> let lat = latitudeOf pos in (min minLat lat, max maxLat lat))
        (fstLat, fstLat)
        positions
    where
        fstLat = latitudeOf fstPosition

    (minLng, maxLng) = 'StdEnv'.foldl
        (\(minLng, maxLng) pos -> let lng = longitudeOf pos in (min minLng lng, max maxLng lng))
        (fstLng, fstLng)
        positions
    where
        fstLng = longitudeOf fstPosition
smallestRectCovering _ = ?None

region :: !RegionValue -> ?Region
region val = (\s2Region -> {value = val, s2Region = s2Region}) <$> s2Region
where
	s2Region = case val of
		Poly points` = s2Loop {latitudeOf p \\ p <- points} {longitudeOf p \\ p <- points} (length points)
		where
			points :: [Position]
			points = case mergeConsecutiveElements (\x y -> if (x == y) (?Just x) ?None) points` of
				[fst: rest] | not (isEmpty rest) && fst == last rest = rest
				all                                                  = all
		Circle circle = let (Position pos) = circle.centre in s2Cap pos circle.radius
		Rect rect =
			s2LatLngRect
				(latitudeOf rect.northWest) (longitudeOf rect.northWest)
				(latitudeOf rect.southEast) (longitudeOf rect.southEast)

	mergeConsecutiveElements :: !(a a -> ?a) ![a] -> [a]
	mergeConsecutiveElements mergeFunc [x : rest=:[y : rest`]] = case mergeFunc x y of
		?Just z = mergeConsecutiveElements mergeFunc [z : rest`]
		_       = [x : mergeConsecutiveElements mergeFunc rest]
	mergeConsecutiveElements _ emptyOrSingleton = emptyOrSingleton

entireEarth :: Region
entireEarth =: fromJust $ region $ Rect entireEarthRect

entireEarthRect ::RectRegion
entireEarthRect =:
	{northWest = fromOkWithError $ position 90.0 -180.0, southEast = fromOkWithError $ position -90.0 180.0}

emptyRect :: RectRegion
emptyRect =: {northWest = fromOkWithError $ position 0.0 0.0, southEast = fromOkWithError $ position 0.0 0.0}

regionValueOf :: !Region -> RegionValue
regionValueOf region = region.Region.value

instance == Region where
    (==) x y = x === y

instance < Region where
	(<) x y = x.Region.value < y.Region.value

instance toString Region where
    toString region = toString region.Region.value

instance html Region where
    html region = html region.Region.value

gEq{|Region|}     x y = x.Region.value === y.Region.value
gLexOrd{|Region|} x y = x.Region.value =?= y.Region.value
JSONEncode{|Region|} inField region = JSONEncode{|*|} inField region.Region.value
JSONDecode{|Region|} inField jsonNodes = appFst (maybe ?None region) $ JSONDecode{|*|} inField jsonNodes
gPrint{|Region|} region pst = gPrint{|*|} (regionValueOf region) pst
gPrint{|Position|} position pst = gPrint{|*|} (latitudeOf position, longitudeOf position) pst
gBinaryEncode{|Region|} region st = gBinaryEncode{|*|} region.Region.value st
gBinaryEncodingSize{|Region|} region s = gBinaryEncodingSize{|*|} region.Region.value s
gBinaryDecode{|Region|} st = appFst (maybe ?None region) $ gBinaryDecode{|*|} st

instance == RegionValue where
    (==) x y = x === y

instance < RegionValue where
	(<) x y = (x =?= y) === LT

instance toString RegionValue where
    toString _ = "(some region)"

instance html RegionValue where
    html x = Text $ toString x

instance == RectRegion derive gEq

instance == CircleRegion derive gEq

derive gEq             RegionValue, RectRegion, CircleRegion, LatDirection, LngDirection, Position, S2LatLng
derive gLexOrd         RegionValue, RectRegion, CircleRegion
derive gPrint          RegionValue, RectRegion, CircleRegion, CellId
derive JSONEncode      RegionValue, RectRegion, CircleRegion
derive JSONDecode      RegionValue, RectRegion, CircleRegion
derive class GenBinary RegionValue, RectRegion, CircleRegion, Position, S2LatLng
derive gFDomain        LatDirection, LngDirection

rectRegionToBoundaryLines :: !RectRegion -> LinesForRectRegionBounds
rectRegionToBoundaryLines {northWest, southEast}
	=	{ northWestToNorthEast = line northWestPos northEastPos
		, northEastToSouthEast = line northEastPos southEastPos
		, southWestToSouthEast = line southWestPos southEastPos
		, northWestToSouthWest = line northWestPos southWestPos
		}
where
	// Lat = vertical, lng = horizontal.
	northWestPos = fromOkWithError $ position northLat westLng
	northEastPos = fromOkWithError $ position northLat eastLng
	southWestPos = fromOkWithError $ position southLat westLng
	southEastPos = fromOkWithError $ position southLat eastLng

	northLat = latitudeOf northWest
	southLat = latitudeOf southEast
	eastLng  = longitudeOf southEast
	westLng  = longitudeOf northWest

line :: !Position !Position -> Line
line fstPos=:(Position fstS2LatLng) sndPos=:(Position sndS2LatLng) =
	{value = LineValue [fstPos, sndPos], s2Line = s2Line fstS2LatLng sndS2LatLng}

lineIntersectsWithRectRegionBoundaries :: !Line !LinesForRectRegionBounds -> Bool
lineIntersectsWithRectRegionBoundaries line
	{northWestToNorthEast, northEastToSouthEast, southWestToSouthEast, northWestToSouthWest} =
	linesIntersect line northWestToNorthEast || linesIntersect line northEastToSouthEast ||
	linesIntersect line southWestToSouthEast || linesIntersect line northWestToSouthWest

linesIntersect :: !Line !Line -> Bool
linesIntersect l1 l2 = s2LinesIntersect l1.s2Line l2.s2Line

s2LatLngFromPosition :: !Position -> S2LatLng
s2LatLngFromPosition (Position s2LatLng) = s2LatLng

(isInsideOf) infix 9 :: !Position !Region -> Bool
(isInsideOf) (Position latLng) region = latLngIsInside latLng region.s2Region

areaOf :: !Region -> Real
areaOf region = case region.Region.value of
    Poly   _  = areaOfS2Loop       region.s2Region
    Circle _  = areaOfS2Cap        region.s2Region
    Rect   _  = areaOfS2LatLngRect region.s2Region
