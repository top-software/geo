definition module Data.Geo.GenHash

from Data.GenHash import generic gHash
from Data.Geo import
	:: Position, :: LatDirection, :: LngDirection, :: CellId, :: RegionValue, :: CircleRegion, :: RectRegion, :: Region,
	:: LineValue, :: Line, :: LinesForRectRegionBounds

derive gHash Position
derive gHash LatDirection
derive gHash LngDirection
derive gHash CellId
derive gHash RegionValue
derive gHash CircleRegion
derive gHash RectRegion
derive gHash Region
derive gHash LineValue
derive gHash Line
derive gHash LinesForRectRegionBounds
