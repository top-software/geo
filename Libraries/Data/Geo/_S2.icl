implementation module Data.Geo._S2

import code from library "-lstdc++"
import code from library "-lcrypto"
import code from library "-lpthread"
import code from library "libs2.a"
import code from library "libabsl_strings.a"
import code from library "libabsl_string_view.a"
import code from library "libabsl_throw_delegate.a"
import code from library "libabsl_synchronization.a"
import code from library "libabsl_kernel_timeout_internal.a"
import code from library "libabsl_time.a"
import code from library "libabsl_time_zone.a"
import code from library "libabsl_symbolize.a"
import code from library "libabsl_strings_internal.a"
import code from library "libabsl_str_format_internal.a"
import code from library "libabsl_statusor.a"
import code from library "libabsl_status.a"
import code from library "libabsl_stacktrace.a"
import code from library "libabsl_spinlock_wait.a"
import code from library "libabsl_scoped_set_env.a"
import code from library "libabsl_raw_logging_internal.a"
import code from library "libabsl_raw_hash_set.a"
import code from library "libabsl_random_seed_sequences.a"
import code from library "libabsl_random_seed_gen_exception.a"
import code from library "libabsl_random_internal_seed_material.a"
import code from library "libabsl_random_internal_randen_slow.a"
import code from library "libabsl_random_internal_randen_hwaes_impl.a"
import code from library "libabsl_random_internal_randen_hwaes.a"
import code from library "libabsl_random_internal_randen.a"
import code from library "libabsl_random_internal_pool_urbg.a"
import code from library "libabsl_random_internal_platform.a"
import code from library "libabsl_random_internal_distribution_test_util.a"
import code from library "libabsl_random_distributions.a"
import code from library "libabsl_periodic_sampler.a"
import code from library "libabsl_malloc_internal.a"
import code from library "libabsl_vlog_config_internal.a"
import code from library "libabsl_log_internal_check_op.a"
import code from library "libabsl_log_internal_nullguard.a"
import code from library "libabsl_log_internal_message.a"
import code from library "libabsl_log_internal_proto.a"
import code from library "libabsl_log_internal_structured_proto.a"
import code from library "libabsl_log_internal_log_sink_set.a"
import code from library "libabsl_log_internal_format.a"
import code from library "libabsl_log_internal_fnmatch.a"
import code from library "libabsl_log_internal_globals.a"
import code from library "libabsl_log_globals.a"
import code from library "libabsl_log_severity.a"
import code from library "libabsl_log_sink.a"
import code from library "libabsl_strerror.a"
import code from library "libabsl_leak_check.a"
import code from library "libabsl_int128.a"
import code from library "libabsl_hashtablez_sampler.a"
import code from library "libabsl_hash.a"
import code from library "libabsl_low_level_hash.a"
import code from library "libabsl_graphcycles_internal.a"
import code from library "libabsl_flags_usage_internal.a"
import code from library "libabsl_flags_usage.a"
import code from library "libabsl_flags_reflection.a"
import code from library "libabsl_flags_private_handle_accessor.a"
import code from library "libabsl_flags_parse.a"
import code from library "libabsl_flags_marshalling.a"
import code from library "libabsl_flags_internal.a"
import code from library "libabsl_flags_config.a"
import code from library "libabsl_flags_program_name.a"
import code from library "libabsl_flags_commandlineflag_internal.a"
import code from library "libabsl_flags_commandlineflag.a"
import code from library "libabsl_failure_signal_handler.a"
import code from library "libabsl_exponential_biased.a"
import code from library "libabsl_examine_stack.a"
import code from library "libabsl_debugging_internal.a"
import code from library "libabsl_demangle_internal.a"
import code from library "libabsl_demangle_rust.a"
import code from library "libabsl_decode_rust_punycode.a"
import code from library "libabsl_utf8_for_code_point.a"
import code from library "libabsl_cordz_sample_token.a"
import code from library "libabsl_cordz_info.a"
import code from library "libabsl_cordz_handle.a"
import code from library "libabsl_cordz_functions.a"
import code from library "libabsl_cord_internal.a"
import code from library "libabsl_cord.a"
import code from library "libabsl_civil_time.a"
import code from library "libabsl_city.a"
import code from library "libabsl_base.a"
import code from library "libabsl_bad_variant_access.a"
import code from library "libabsl_bad_optional_access.a"
import code from library "libabsl_bad_any_cast_impl.a"
import code from "s2Interface.o"
import StdEnv
import Data._Array
import System._Finalized, System._Pointer, System._Posix, StdMisc, StdInt, StdClass, Data.Func, StdFunc
import Data.GenDefault
from Data.Functor import class Functor(fmap)
import System.SysCall
import System._Unsafe

s2LatLng :: !Real !Real -> S2LatLng
s2LatLng lat lng
    # res = s2LatLng` lat lng latLngData
    | res <> res = undef
    = S2LatLng latLngData
where
    latLngData = unsafeCreateArray sizeOfS2LatLng

    s2LatLng` :: !Real !Real !{#Char} -> Int
    s2LatLng` lat lng latLngData = code {
        ccall s2LatLng "RRs:I"
    }

sizeOfS2LatLng :: Int
sizeOfS2LatLng = code {
    ccall sizeOfS2LatLng ":I"
}

s2LatLngGetLat :: !S2LatLng -> Real
s2LatLngGetLat latLng = code {
    ccall s2LatLngGetLat "s:R"
}

s2LatLngGetLng :: !S2LatLng -> Real
s2LatLngGetLng latLng = code {
    ccall s2LatLngGetLng "s:R"
}

s2Loop :: !{#Real} !{#Real} !Int -> ?S2RegionPtr
s2Loop lats lngs n
	| ptr == 0  = ?None
	| otherwise = ?Just $ S2RegionPtr (finalizeInt ptr deleteS2RegionPtr)
where
	ptr = s2Loop` lats lngs n

	s2Loop` :: !{#Real} !{#Real} !Int -> Pointer
	s2Loop` lats lngs n = code {
		ccall s2Loop "AAI:p"
	}

s2Cap :: !S2LatLng !Real -> ?S2RegionPtr
s2Cap center radius
	| ptr == 0  = ?None
	| otherwise = ?Just $ S2RegionPtr (finalizeInt ptr deleteS2RegionPtr)
where
	ptr = s2Cap` center radius

	s2Cap` :: !S2LatLng !Real -> Pointer
	s2Cap` center radiusd = code {
		ccall s2Cap "sR:p"
	}

s2LatLngRect :: !Real !Real !Real !Real -> ?S2RegionPtr
s2LatLngRect northLat westLng southLat eastLng
	| ptr == 0  = ?None
	| otherwise = ?Just $ S2RegionPtr (finalizeInt ptr deleteS2RegionPtr)
where
	ptr = s2LatLngRect` northLat westLng southLat eastLng

	s2LatLngRect` :: !Real !Real !Real !Real -> Pointer
	s2LatLngRect` northLat westLng southLat eastLng = code {
		ccall s2LatLngRect "RRRR:p"
	}

deleteS2RegionPtr :: Pointer
deleteS2RegionPtr = code {
    ccall deleteS2RegionPtr ":p"
}

deleteS2LinePtr :: Pointer
deleteS2LinePtr = code {
    ccall deleteS2LinePtr ":p"
}

distanceBetweenLatLng :: !S2LatLng !S2LatLng -> Real
distanceBetweenLatLng x y = code {
    ccall distanceBetweenLatLng "ss:R"
}

latLngIsInside :: !S2LatLng !S2RegionPtr -> Bool
latLngIsInside latLng (S2RegionPtr fptr) = fst $ withFinalizedInt (latLngIsInside` latLng) fptr
where
    latLngIsInside` :: !S2LatLng !Pointer -> Bool
    latLngIsInside` latLng region = code {
        ccall latLngIsInside "sp:I"
    }

cellIdOfLatLng :: !S2LatLng -> {#Char}
cellIdOfLatLng pos
    # res = cellIdOfLatLng` pos cellIdDest
    | res <> res = undef
    = cellIdDest
where
    // cell id is 64-bit unsigned integer
    cellIdDest = unsafeCreateArray 8

    cellIdOfLatLng` :: !S2LatLng !{#Char} -> Int
    cellIdOfLatLng` pos cellIdDest = code {
        ccall cellIdOfLatLng "ss:I"
    }

coveringCellIdRanges :: !S2RegionPtr -> [(String, String)]
coveringCellIdRanges (S2RegionPtr fptr) =
    fst $ withFinalizedInt (getResultAndFree readRanges o coveringCellIdRanges`) fptr
where
    // result is an array [nr of ranges (uint16), l_0, u_0, l_1, u_1]
    // array has to be freed after usage
    coveringCellIdRanges` :: !Pointer -> Pointer
    coveringCellIdRanges` region = code {
        ccall coveringCellIdRanges "p:p"
    }

readRanges :: !Pointer -> [(String, String)]
readRanges ptr
    # n   = readInt2Z ptr 0
    # ptr = ptr + 2 // goto beginning of ranges
    = [(derefCharArray (ptr + i*16) 8, derefCharArray (ptr + i*16 + 8) 8) \\ i <- [0..n - 1]]

getResultAndFree :: !(Pointer -> a) !Pointer -> a
getResultAndFree f ptr
    # (r, ptr) = readP (hyperstrict o f) ptr // force eval with hyperstrict, such that freeing is safe
    # x = appUnsafe (\env = free ptr env) 0
    | x <> x = undef
    = r

areaOfS2Loop :: !S2RegionPtr -> Real
areaOfS2Loop (S2RegionPtr ptr) = fst $ withFinalizedInt areaOfS2Loop` ptr
where
    areaOfS2Loop` :: !Pointer -> Real
    areaOfS2Loop` region = code {
        ccall areaOfS2Loop "p:R"
    }

areaOfS2Cap :: !S2RegionPtr -> Real
areaOfS2Cap (S2RegionPtr ptr) = fst $ withFinalizedInt areaOfS2Cap` ptr
where
    areaOfS2Cap` :: !Pointer -> Real
    areaOfS2Cap` region = code {
        ccall areaOfS2Cap "p:R"
    }

areaOfS2LatLngRect :: !S2RegionPtr -> Real
areaOfS2LatLngRect (S2RegionPtr ptr) = fst $ withFinalizedInt areaOfS2LatLngRect` ptr
where
    areaOfS2LatLngRect` :: !Pointer -> Real
    areaOfS2LatLngRect` region = code {
        ccall areaOfS2LatLngRect "p:R"
    }


s2Line :: !S2LatLng !S2LatLng -> S2LinePtr
s2Line (S2LatLng fstLatLng) (S2LatLng sndLatLng)
    # ptr = s2Line` fstLatLng sndLatLng
    = ptrToS2LinePtr ptr
where
    s2Line` :: !{#Char} !{#Char} -> Pointer
    s2Line` fstLatLng sndLatLng = code {
        ccall s2Line "ss:p"
    }

    ptrToS2LinePtr :: !Pointer -> S2LinePtr
    ptrToS2LinePtr ptr = S2LinePtr (finalizeInt ptr deleteS2LinePtr)

s2LinesIntersect :: !S2LinePtr !S2LinePtr -> Bool
s2LinesIntersect (S2LinePtr fstPtr) (S2LinePtr sndPtr)
    = fst3 $ withFinalizedInt2 s2LinesIntersect` fstPtr sndPtr
where
    s2LinesIntersect` :: !Pointer !Pointer -> Bool
    s2LinesIntersect` p1 p2 = code {
        ccall s2LinesIntersect "pp:I"
    }

gDefault{|S2LatLng|} = s2LatLng 0.0 0.0
