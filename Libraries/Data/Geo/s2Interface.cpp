#include <cstdint>

#include <s2/s2latlng.h>
#include <s2/s2earth.h>
#include <s2/s2cap.h>
#include <s2/s2latlng_rect.h>
#include <s2/s2region_coverer.h>
#include <s2/s2loop.h>
#include <s2/s2polyline.h>

extern"C" {

double distanceBetweenLatLng(const S2LatLng* x, const S2LatLng* y) {
    const S1Angle dst = x->GetDistance(*y);
    return S2Earth::ToMeters(dst);
}

int latLngIsInside(const S2LatLng* latLng, const S2Region* region) {
    return region->Contains(latLng->ToPoint());
}

void cellIdOfLatLng(const S2LatLng* pt, uint64_t* cellId) {
    const S2CellId id = S2CellId(*pt);
    *cellId = __builtin_bswap64(id.id());
}

void* coveringCellIdRanges(const S2Region* region) {
    S2RegionCoverer coverer{S2RegionCoverer::Options()};
    const S2CellUnion cells = coverer.GetCovering(*region);
    const uint16_t nCells = cells.num_cells();

    // store nr of ranges in first 16-bit
    // for each range 128-bits are required (two 64-bit bounds)
    void* result = malloc(2 + 16 * nCells);
    *(uint16_t*)result = nCells;

    // store ranges after first 16-bit
    uint64_t* ranges = (uint64_t*)((uint16_t*)result + 1);
    for (uint16_t i = 0; i < nCells; i++) {
        const S2CellId cell = cells.cell_id(i);
        ranges[i*2]     = __builtin_bswap64(cell.range_min().id());
        ranges[i*2 + 1] = __builtin_bswap64(cell.range_max().id());
    }

    return result;
}

void s2LatLng(const double lat, const double lng, S2LatLng* latLng) {
    *latLng = S2LatLng::FromDegrees(lat, lng);
}

size_t sizeOfS2LatLng() { return sizeof(S2LatLng); }
double s2LatLngGetLat(const S2LatLng* latLng) { return latLng->lat().degrees(); }
double s2LatLngGetLng(const S2LatLng* latLng) { return latLng->lng().degrees(); }

S2Loop* s2Loop(const double* polyLats, const double* polyLngs, const int n) {
    std::vector<S2Point> vertices;

    for (uint i = 0; i < n; i++) {
        vertices.push_back(S2LatLng::FromDegrees(polyLats[i], polyLngs[i]).ToPoint());
    }

    S2Loop* loop = new S2Loop(vertices);
    if (!loop -> IsValid()) {delete loop; return NULL;}
    loop->Normalize();

    return loop;
}

S2Cap* s2Cap(const S2LatLng* center, const double radius) {
    if (radius < 0.0) return NULL;
    const S1Angle rad = S1Angle::Radians(S2Earth::MetersToRadians(radius));
    S2Cap* cap = new S2Cap(center->ToPoint(), rad);
    if (!cap -> is_valid()) {delete cap; return NULL;}
    return cap;
}

S2LatLngRect* s2LatLngRect(const double northLat, const double westLng, const double southLat, const double eastLng) {
	S2LatLngRect* rect =
		new S2LatLngRect(S2LatLng::FromDegrees(southLat, westLng), S2LatLng::FromDegrees(northLat, eastLng));
    if (!rect -> is_valid()) {delete rect; return NULL;}
	return rect;
}

void deleteS2Region(S2Region* ptr) {
    delete ptr;
}

void (*deleteS2RegionPtr())(S2Region*) {
    return &deleteS2Region;
}

S2Polyline* s2Line (const S2LatLng* fstLatLng, const S2LatLng* sndLatLng) {
    const S2LatLng latLngs[2] = {*fstLatLng, *sndLatLng};
    return new S2Polyline(absl::Span<const S2LatLng>(latLngs));
}

int s2LinesIntersect (const S2Polyline* fstLine, const S2Polyline* sndLine) {
    return fstLine->Intersects(*sndLine);
}

void deleteS2Line(S2Polyline* ptr) {
    delete ptr;
}

void (*deleteS2LinePtr())(S2Polyline*) {
    return &deleteS2Line;
}

double areaOfS2Loop(S2Loop* loop) {
    return S2Earth::SteradiansToSquareMeters(loop->GetArea());
}

double areaOfS2Cap(S2Cap* cap) {
    return S2Earth::SteradiansToSquareMeters(cap->GetArea());
}

double areaOfS2LatLngRect(S2LatLngRect* rect) {
    return S2Earth::SteradiansToSquareMeters(rect->Area());
}

}
