implementation module Data.Geo.Gast

import Control.GenBimap
import Data.Error, Data.Functor, Data.List, Data.Maybe
import Gast, Gast.Gen
import StdEnv, StdOverloadedList

import Data.Geo

genShow{|Position|} sep p position rest = genShow{|*|} sep p (latitudeOf position, longitudeOf position) rest

ggen{|Position|} _ =
	[! fromOkWithError $ position lat lng
	\\ (lat, lng)
	<- diag2
		[0.0,  -90.0,  90.0: (\x -> toReal x / 10000.0) <$> [-10..10]]
		[0.0, -180.0, 180.0: (\x -> toReal x / 10000.0) <$> [-10..10]]
	]

derive genShow CellId, RegionValue, RectRegion, CircleRegion

genShow{|Region|} sep p region rest = genShow{|*|} sep p (regionValueOf region) rest

ggen{|CellId|} st = (\(c1, c2, c3, c4, c5, c6, c7, c8) -> CellId {c1, c2, c3, c4, c5, c6, c7, c8}) <$> ggen{|*|} st

derive ggen RegionValue

ggen{|RectRegion|} st =
	[! {northWest = northWest, southEast = southEast} \\ (northWest, southEast) <|- ggen{|*|} st
	|  latitudeOf southEast <= latitudeOf northWest && longitudeOf northWest <= longitudeOf southEast ]

ggen{|CircleRegion|} st = [!{centre = centre, radius = radius} \\ (centre, radius) <|- ggen{|*|} st | isFinite radius]

ggen{|Region|} gst = [!r \\ ?Just r <|- region <$> ggen{|*|} gst]
