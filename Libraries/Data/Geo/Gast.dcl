definition module Data.Geo.Gast

from Data.Geo import :: Position, :: CellId, :: RegionValue, :: Region, :: RectRegion, :: CircleRegion
from Gast     import generic ggen, generic genShow, :: GenState

derive genShow Position, CellId, RegionValue, Region, RectRegion, CircleRegion

/**
 * This generates positions such that the distance between most consecutive positions,
 * is a distance which objects (such as cars, ships, ...) can realistically move in 1 second.
 */
derive ggen Position

derive ggen CellId, RegionValue, Region, RectRegion, CircleRegion
