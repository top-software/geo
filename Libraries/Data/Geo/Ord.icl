implementation module Data.Geo.Ord

import StdEnv

import Data.Geo

instance < CircleRegion where
	(<) c0 c1 = if (c0.radius == c1.radius) (c0.centre < c1.centre) (c0.radius < c1.radius)
