implementation module Data.Geo.GenHash

import Data.GenHash, Data.Geo
import StdEnv

gHash{|Position|} pos = gHash{|*|} (latitudeOf pos, longitudeOf pos)
derive gHash LatDirection
derive gHash LngDirection
derive gHash CellId
derive gHash RegionValue
derive gHash CircleRegion
derive gHash RectRegion
gHash{|Region|} {Region| value} = gHash{|*|} value
derive gHash LineValue
gHash{|Line|} {Line| value} = gHash{|*|} value
derive gHash LinesForRectRegionBounds
