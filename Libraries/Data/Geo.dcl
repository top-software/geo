definition module Data.Geo

/**
 * A module for dealing with positions and areas on the earth's surface.
 *
 * @property-bootstrap
 *     import Control.Monad
 *     import Data.Geo.Gast
 *     import Data.Error, Data.Functor, Data.Maybe
 *     from Data.List import intersect, difference
 *     import Gast.Gen
 *     import StdEnv
 *     import Text.GenPrint
 *
 *     import Data.Geo.Gast
 *
 *     secondsPrecision = 2
 */

from Data.Encoding.GenBinary import
    class GenBinary, generic gBinaryEncode, generic gBinaryDecode, generic gBinaryEncodingSize, :: EncodingSt
from Data.Error import :: MaybeError
from Data.Func import $
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Data.GenFDomain import generic gFDomain
from Data.GenLexOrd import generic gLexOrd, :: LexOrd
from StdOverloaded import class <, class ==, class toString
from Text.GenPrint import generic gPrint, class PrintOutput, :: PrintState
from Text.HTML import class html
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

from Data.Geo._S2 import :: S2RegionPtr, :: S2LinePtr
from Data.Geo._S2 import :: S2LatLng

/**
 * A position on the earth's surface.
 * For performance reasons all operations are performed on values of type `Position`
 * to which values of this type can be converted.
 *
 * Should be abstract, but has to be used together with dynamics.
 */
:: Position =: Position S2LatLng

//* The number of decimals the latitude/longitude of positions in degrees is restricted to.
POSITION_PRECISION :== 10000000.0

/**
 * position lat lng = pos:
 *     `pos` is the position given by `lat` and `lng` in degrees.
 */
position :: !Real !Real -> MaybeError String Position

/**
 * positionCorrespondingToHoursMinutesSeconds
 *     latHours latMinutes latSeconds latPrecision latDirection
 *     lngHours lngMinutes lngSeconds lngPrecision lngDirection
 * = position:
 *     `position` is the position corresponding to the hours/minutes/seconds components given as argument.
 *     The latitude second component is `latSeconds E -latPrecision`; vice versa for the longitude component.
 *
 * @property to/from equivalence: A.pos :: Position:
 *         fromOkWithError (positionCorrespondingToHoursMinutesSeconds
 *             latH latM latS secondsPrecision latDir lngH lngM lngS secondsPrecision lngDir)
 *     =.=
 *         pos
 *     where
 *         (latH, latM, latS, latDir) = latitudeHoursMinutesSecondsOf  secondsPrecision pos
 *         (lngH, lngM, lngS, lngDir) = longitudeHoursMinutesSecondsOf secondsPrecision pos
 */
positionCorrespondingToHoursMinutesSeconds ::
	!Int !Int !Int !Int !LatDirection !Int !Int !Int !Int !LngDirection -> MaybeError String Position

/**
 * latitudeOf position = lat:
 *     `lat` is the latitude of `position` in degrees.
 *
 * @property is in expected range: A.pos :: Position:
 *     let lat = latitudeOf pos in lat >=. -90.0 /\ lat <=. 90.0
 */
latitudeOf :: !Position -> Real

/**
 * longitudeOf position = lng:
 *     `lng` is the longitude of `position` in degrees.
 *
 * @property is in expected range: A.pos :: Position:
 *     let lng = longitudeOf pos in lng >=. -180.0 /\ lng <=. 180.0
 */
longitudeOf :: !Position -> Real

/**
 * latitudeHoursMinutesSecondsOf precision position = (hours, minutes, seconds, direction):
 *     `(hours, minutes, seconds, direction)` are the latitude components corresponding to `position`.
 *     The second component is `seconds E -precision`.
 *
 * @property results are in expected range: A.pos :: Position:
 *     hours >. -90 /\ hours <=. 90 /\ mins >=. 0 /\ mins <. 60 /\
 *     seconds >=. 0 /\ seconds <. 60 * 10^secondsPrecision
 *     where
 *         (hours, mins, seconds, _) = latitudeHoursMinutesSecondsOf secondsPrecision pos
 */
latitudeHoursMinutesSecondsOf :: !Int !Position -> (!Int, !Int, !Int, !LatDirection)

/**
 * longitudeHoursMinutesSecondsOf position = (hours, minutes, seconds, direction):
 *     `(hours, minutes, seconds, direction)` are the longitude components corresponding to `position`.
 *     The second component is `seconds E -precision`.
 *
 * @property results are in expected range: A.pos :: Position:
 *     hours >. -180 /\ hours <=. 180 /\ mins >=. 0 /\ mins <. 60 /\
 *     seconds >=. 0 /\ seconds <. 60 * 10^secondsPrecision
 *     where
 *         (hours, mins, seconds, _) = longitudeHoursMinutesSecondsOf secondsPrecision pos
 */
longitudeHoursMinutesSecondsOf :: !Int !Position -> (!Int, !Int, !Int, !LngDirection)

:: LatDirection = North | South

instance toString LatDirection

derive gEq      LatDirection
derive gFDomain LatDirection

:: LngDirection = East | West

instance toString LngDirection

derive gEq      LngDirection
derive gFDomain LngDirection

/**
 * https://s2geometry.io/devguide/s2cell_hierarchy
 */
:: CellId =: CellId {#Char}

/**
 * @property is reflexive: A.a :: CellId:
 *     a =.= a
 * @property is symmetric: A.a :: CellId; b :: CellId:
 *     a =.= b ==> b =.= a
 * @property is transitive: A.a :: CellId; b :: CellId; c :: CellId:
 *     a =.= b /\ b =.= c ==> a =.= c
 */
instance == CellId :: !CellId !CellId -> Bool :== code {.d 2 0 ; jsr eqAC ; .o 0 1 b}

/**
 * @property is transitive: A.a :: CellId; b :: CellId; c :: CellId:
 *     a <. b /\ b <. c ==> a <. c
 * @property is reversible: A.a :: CellId; b :: CellId:
 *     (a <. b) <==> (b >. a)
 */
instance <  CellId :: !CellId !CellId -> Bool :== code {.d 2 0 ; jsr cmpAC ; .o 0 1 i ; pushI 0 ; gtI}

derive gPrint CellId

idOfCellIncluding :: !Position -> CellId

/**
 * longitudeInTheMiddleBetween lng1 lng2 = midLng:
 *     `midLng` is the smallest of both longitudes in the middle between `lng1` and `lng2`.
 *     The are always two points in the middle at opposite sides of the earth.
 *
 * @property is in expected range: A.lng1 :: Real; lng2 :: Real:
 *     let lng = longitudeInTheMiddleBetween lng1 lng2 in lng >. -180.0 /\ lng <=. 180.0
 * @precondition lng1 > -180.0 && lng1 <= 180.0 && lng2 > -180.0 && lng2 <= 180.0
 */
longitudeInTheMiddleBetween :: !Real !Real -> Real

/**
 * @property is reflexive: A.a :: Position:
 *     a =.= a
 * @property is symmetric: A.a :: Position; b :: Position:
 *     a =.= b ==> b =.= a
 * @property is transitive: A.a :: Position; b :: Position; c :: Position:
 *     a =.= b /\ b =.= c ==> a =.= c
 */
instance == Position :: !Position !Position -> Bool :== code {.d 2 0 ; jsr eqAC ; .o 0 1 b}

/** The order is based on `(latitude, longitude)`.
 *
 * @property is transitive: A.a :: Position; b :: Position; c :: Position:
 *     a <. b /\ b <. c ==> a <. c
 * @property is reversible: A.a :: Position; b :: Position:
 *     (a <. b) <==> (b >. a)
 */
instance < Position

instance toString   Position
instance html       Position
derive   gEq        Position
derive   gLexOrd    Position
derive   JSONEncode Position
derive   JSONDecode Position

/**
 * distanceBetween pos1 pos2 = distance:
 *     `distance` is the distance between `pos1` and `pos2` in meters.
 *
 * @property is non-negative: A.pos1 :: Position; pos2 :: Position:
 *     distanceBetween pos1 pos2 >=. 0.0
 * @property obeys triangle inequality: A.pos1 :: Position; pos2 :: Position; pos3 :: Position:
 *     distanceBetween pos1 pos3 <=. distanceBetween pos1 pos2 + distanceBetween pos2 pos3 + allowedError
 *     where
 *         allowedError = 1.0E-3 // allow error of 1 milimeter
 */
distanceBetween :: !Position !Position -> Real

/**
 * This represents a region on the earth's surface.
 * For performance reasons all operations are performed on values of type `Region`
 * to which values of this type can be converted.
 *
 * @cons `Poly corners` represents a polygon with corners `corners`.
 * @cons `Circle circle` represents a circle defined by `circle`.
 *       The radius must at least be 0.01.
 * @cons `Rect rect` represents a rectangular region with given corner points.
 */
:: RegionValue = Poly   ![Position]
               | Circle !CircleRegion
               | Rect   !RectRegion

/**
 * @property is transitive: A.a :: RegionValue; b :: RegionValue; c :: RegionValue:
 *     a <. b /\ b <. c ==> a <. c
 * @property is reversible: A.a :: RegionValue; b :: RegionValue:
 *     (a <. b) <==> (b >. a)
 */
instance < RegionValue

:: CircleRegion =
	{ centre :: !Position
	, radius :: !Real     //* radius (in meters)
	}

/**
 * @property is reflexive: A.a :: CircleRegion:
 *     a =.= a
 * @property is symmetric: A.a :: CircleRegion; b :: CircleRegion:
 *     a =.= b ==> b =.= a
 * @property is transitive: A.a :: CircleRegion; b :: CircleRegion; c :: CircleRegion:
 *     a =.= b /\ b =.= c ==> a =.= c
 */
instance == CircleRegion

//* A rectangular region
:: RectRegion = {northWest :: !Position, southEast :: !Position}

/**
 * @property is reflexive: A.a :: RectRegion:
 *     a =.= a
 * @property is symmetric: A.a :: RectRegion; b :: RectRegion:
 *     a =.= b ==> b =.= a
 * @property is transitive: A.a :: RectRegion; b :: RectRegion; c :: RectRegion:
 *     a =.= b /\ b =.= c ==> a =.= c
 */
instance == RectRegion

derive gEq                 RectRegion, CircleRegion
derive JSONEncode          RectRegion, CircleRegion
derive JSONDecode          RectRegion, CircleRegion
derive gPrint              Position, Region, RectRegion, CircleRegion, RegionValue
derive gDefault Position
derive gBinaryEncode       Region
derive gBinaryEncodingSize Region
derive gBinaryDecode       Region
derive class GenBinary     Position, RectRegion, CircleRegion, RegionValue

/**
 * smallestRectCovering [] = ?None
 * smallestRectCovering positions = ?Just rect:
 *     `rectRegion` is the smallest rect region covering all `positions`.
 *     The west longitude is smaller than the east longitude.
 */
smallestRectCovering :: ![Position] -> ?RectRegion

/**
 * region value = ?Just a: `a` is the region corresponding to `value`.
 * region value = ?None: `value` does not represent a valid region (e.g. a polygon with crossing edges).
 */
region :: !RegionValue -> ?Region

/**
 * Abbreviation for defining polygons.
 *
 * @type [Position] -> Region
 */
polygon points :== region $ Poly points

/**
 * Abbreviation For defining rects.
 *
 * @type RectRegion -> Region
 */
rect r :== region $ Rect r

/**
 * Abbreviation for defining circular regions.
 *
 * @type Position Real -> Region
 */
circle pos radius :== region $ Circle {centre = pos, radius = radius}

/**
 * The region containing the entire earth.
 */
entireEarth :: Region

//* The rect region values containing the entire earth.
entireEarthRect ::RectRegion

//* The rect region values containing no points.
emptyRect :: RectRegion

/**
 * regionValueOf region = value:
 *     `value` is the region value corresponding to `region`.
 */
regionValueOf :: !Region -> RegionValue

// should be abstract, but has to be used with dynamics
:: Region = { value    :: !RegionValue   //* the actual region value
            , s2Region :: !S2RegionPtr //* a pointer to the S2 region representation, used for performance reasons
            }

//* Contains the positions which form a line when a line is drawn between the positions.
:: LineValue =: LineValue [Position]

//* A line, can be seen as two points that have a line drawn between the points.
:: Line =
    { value :: !LineValue //* The position values of the line.
    , s2Line :: !S2LinePtr //* A pointer representing the line for making use of the S2 library.
    }

//* Record which holds four lines which represent the boundaries of a `:: RectRegion`
:: LinesForRectRegionBounds =
    { northWestToNorthEast :: !Line //* Line from the north west to north east corner of the `:: RectRegion`.
    , northEastToSouthEast :: !Line //* Line from the north east to south east corner of the `:: RectRegion`.
    , southWestToSouthEast :: !Line //* Line from the south west to south east corner of the `:: RectRegion`.
    , northWestToSouthWest :: !Line //* Line from the north west to south west corner of the `:: RectRegion`.
    }

/**
 * Convert a term of `:: RectRegion` to lines which represent the bounds of the `:: RectRegion`.
 *
 * @property correctness: A.northWest :: Position; southEast :: Position:
 *  let
 *      northLat = latitudeOf northWest
 *      southLat = latitudeOf southEast
 *      eastLng  = longitudeOf southEast
 *      westLng  = longitudeOf northWest
 *      northWestPos = fromOkWithError $ position northLat westLng
 *      northEastPos = fromOkWithError $ position northLat eastLng
 *      southWestPos = fromOkWithError $ position southLat westLng
 *      southEastPos = fromOkWithError $ position southLat eastLng
 *      rectRegion = {northWest = northWestPos, southEast = southEastPos}
 *      {northWestToNorthEast, northEastToSouthEast, southWestToSouthEast, northWestToSouthWest}
 *          = rectRegionToBoundaryLines rectRegion
 *      (LineValue northWestToNorthEastPositions) = northWestToNorthEast.Line.value
 *      (LineValue northEastToSouthEastPositions) = northEastToSouthEast.Line.value
 *      (LineValue southWestToSouthEastPositions) = southWestToSouthEast.Line.value
 *      (LineValue northWestToSouthWestPositions) = northWestToSouthWest.Line.value
 *      northWestToNorthEastLatLngs = map posToLatLng northWestToNorthEastPositions
 *      northEastToSouthEastLatLngs = map posToLatLng northEastToSouthEastPositions
 *      southWestToSouthEastLatLngs = map posToLatLng southWestToSouthEastPositions
 *      northWestToSouthWestLatLngs = map posToLatLng northWestToSouthWestPositions
 *  in
 *  isEmpty (difference northWestToNorthEastLatLngs [(northLat, westLng), (northLat, eastLng)]) /\
 *  isEmpty (difference northEastToSouthEastLatLngs [(northLat, eastLng), (southLat, eastLng)]) /\
 *  isEmpty (difference southWestToSouthEastLatLngs [(southLat, westLng), (southLat, eastLng)]) /\
 *  isEmpty (difference northWestToSouthWestLatLngs [(northLat, westLng), (southLat, westLng)])
 *  where
 *      posToLatLng :: !Position -> (!Real, !Real)
 *      posToLatLng pos = (latitudeOf pos, longitudeOf pos)
 */
rectRegionToBoundaryLines :: !RectRegion -> LinesForRectRegionBounds

/**
 * Create a line between two positions.
 */
line :: !Position !Position -> Line

/**
 * Returns whether any of the lines which represent the boundaries of a `:: RectRegion` intersect with a line.
 *
 * @param The line
 * @param The lines which represent the boundaries of a `:: RectRegion`, see `rectRegionToBoundaryLines`.
 * @result Whether any boundary line intersects with the given line.
 */
lineIntersectsWithRectRegionBoundaries :: !Line !LinesForRectRegionBounds -> Bool

/**
 * Returns whether two lines intersect with each other.
 *
 * @param The first line.
 * @param The second line.
 * @result Whether the lines intersect.
 * @property lines always intersect if they share a common position: A.pos1 :: Position; pos2 :: Position; pos3 :: Position; pos4 :: Position:
 *  let
 *      line1 = line pos1 pos2
 *      line2 = line pos3 pos4
 *  in
 *     (not $ isEmpty $ intersect [pos1, pos2] [pos3, pos4]) ==> linesIntersect line1 line2
 */
linesIntersect :: !Line !Line -> Bool

/**
 * @property is reflexive: A.a :: Region:
 *     a =.= a
 * @property is symmetric: A.a :: Region; b :: Region:
 *     a =.= b ==> b =.= a
 * @property is transitive: A.a :: Region; b :: Region; c :: Region:
 *     a =.= b /\ b =.= c ==> a =.= c
 */
instance == Region

/**
 * @property is reflexive: A.a :: RegionValue:
 *     a =.= a
 * @property is symmetric: A.a :: RegionValue; b :: RegionValue:
 *     a =.= b ==> b =.= a
 * @property is transitive: A.a :: RegionValue; b :: RegionValue; c :: RegionValue:
 *     a =.= b /\ b =.= c ==> a =.= c
 */
instance == RegionValue

/**
 * @property is transitive: A.a :: Region; b :: Region; c :: Region:
 *     a <. b /\ b <. c ==> a <. c
 * @property is reversible: A.a :: Region; b :: Region:
 *     (a <. b) <==> (b >. a)
 */
instance < Region

instance toString   Region, RegionValue
instance html       Region, RegionValue
derive   gEq        Region, RegionValue
derive   gLexOrd    Region, RegionValue
derive   JSONEncode Region, RegionValue
derive   JSONDecode Region, RegionValue

/**
 * pos isInsideOf region = True:
 *     `pos` is inside of `region`.
 * pos isInsideOf region = False:
 *     `pos` is not inside of `region`.
 */
(isInsideOf) infix 9:: !Position !Region -> Bool

/**
 * areaOf region = area:
 *     `area` is the area of `region` in m^2.
 */
areaOf :: !Region -> Real
