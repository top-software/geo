#! /bin/bash

set -e

apt-get update -qq
apt-get upgrade -qq
apt-get install -qq --no-install-recommends build-essential cmake googletest libssl-dev
